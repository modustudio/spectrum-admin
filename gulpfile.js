/*
npm i -D gulp gulp-plumber@1.2.0 gulp-connect@5.0.0 gulp-ejs-locals@0.0.1 gulp-sass@3.1.0 gulp-autoprefixer@4.1.0 gulp-jshint@2.1.0 jshint-stylish@2.2.1 gulp-watch@5.0.0 gulp-clean@0.4.0 gulp-prettify@0.5.0
npm i -D gulp-imagemin@4.1.0
npm i -D imagemin-pngquant@5.0.1
*/

var gulp = require('gulp'),
  clean = require('gulp-clean'),
  watch = require('gulp-watch'),
  plumber = require('gulp-plumber'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  connect = require('gulp-connect'),
  ejslocals = require('gulp-ejs-locals'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  prettify = require('gulp-prettify')

var fs = require('fs');

// Path
var src = './public/src';
var dist = './public/dist';

// Server
gulp.task('serve', function() {
  connect.server({
    root: dist,
    port: 8000,
    livereload: true,
    open: {
      browser: 'chrome'
    }
  });
});

//sass -> css
gulp.task('sass', function () {
  return gulp.src(src+'/scss/**/*.scss')
  .pipe( plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }) )
  .pipe(sass())
  .pipe(autoprefixer({
    browsers: ['last 3 versions','IE 8','android 2.3'],
    cascade: false
  }))
  .pipe(gulp.dest(dist+'/css'))
  .pipe(connect.reload())
});

//css
gulp.task('css', function () {
  return gulp.src([src+'/css/*.css',  '!' + src+'/css/libs/*.css'])
  .pipe( plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }) )
  .pipe(autoprefixer({
    browsers: ['last 3 versions','IE 8','android 2.3'],
    cascade: false
  }))
  .pipe(gulp.dest(dist+'/css'))
  .pipe(connect.reload())
});
// ejs -> HTML
gulp.task('ejs', function() {
  gulp
    .src([src+'/ejs/**/*.ejs',  '!' + src+'/ejs/**/_*.ejs'])
		
    .pipe(ejslocals(
      {jsonData: JSON.parse(fs.readFileSync(src+'/ejs/index.json'))},
      {ext: '.html'}
    ))
    .pipe(prettify({indent_size: 1, indent_char:'\t'}))
    .pipe( plumber({
	      errorHandler: function (error) {
	        console.log(error.message);
	        this.emit('end');
	      }
	    }) )
    .pipe(gulp.dest(dist+'/html'))
    .pipe(connect.reload())
});

// js hint
gulp.task('js:hint', function () {
  gulp
    .src([src+'/js/*.js',  '!' + src+'/js/libs/*.js'])
    .pipe(gulp.dest(dist+'/js'))
    .pipe(connect.reload())
});

// image
gulp.task('imgmin', function() {
  gulp
    .src([src+'/images/**/*.{png,jpg,gif}'])
    // .pipe(imagemin({
    //   progressive: true,
    //     interlaced:true,
    //   use: [pngquant()]
    // }))
    .pipe(gulp.dest(dist+'/images'))
    .pipe(connect.reload())
});

// font
gulp.task('font', function () {
  gulp
    .src([src+'/fonts/**/*'])
    .pipe(gulp.dest(dist+'/fonts'))
});

// library
gulp.task('libs', function () {
  gulp
    .src([src+'/libs/**/*', '!' + src+'/libs/**/scss/**/*'])
    .pipe(gulp.dest(dist+'/libs'))
    .pipe(connect.reload())
});

//bootstrap
gulp.task('bootstrap', function () {
  return gulp.src(src+'/libs/bootstrap/scss/**/*.scss')
  .pipe( plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }) )
  .pipe(sass())
  .pipe(gulp.dest(dist+'/libs/bootstrap/css'))
  .pipe(connect.reload())
});

// index
gulp.task('index', function () {
  gulp
    .src([src+'/index.html', src+'/list.html'])
    .pipe(gulp.dest(dist))
});

//clean 작업 설정

gulp.task('clean', function(){
  return gulp.src([dist+'/*',dist+'/*.html','!' + dist+'/.git'], {read: false})
    .pipe(clean());
});


// Watch task
gulp.task('watch',[], function() {
  watch(src+'/ejs/**/*', function() {
    gulp.start('ejs');
  });
  watch(src+'/scss/**/*.{scss,sass}', function() {
    gulp.start(['sass']);
  });
  watch([src+'/css/*.css',  '!' + src+'/css/libs/*.css'], function() {
    gulp.start(['css']);
  });
  watch([src+'/js/*.js',  '!' + src+'/js/libs/*.js'], function() {
    gulp.start('js:hint');
  });
  watch(src+'/images/**/*.{png,jpg,gif}', function() {
    gulp.start('imgmin');
  });
  watch([src+'/index.html', src+'/list.html'], function() {
    gulp.start('index');
  });
  watch([src+'/libs/**/*'], function() {
    gulp.start('libs');
  });
  watch([src+'/libs/bootstrap/scss/**/*.scss'], function() {
    gulp.start('bootstrap');
  });
});

gulp.task('default', ['serve','sass','css','ejs','js:hint','imgmin','watch','font','index','libs','bootstrap']);
