# 스펙트럼 관리자

## *git* 설정
### *local* 환경 구성
*fork* 후 `clone`
```
git clone [url]
```

### *upstream(원본저장소)* 추가
```
git remote add upstream https://gitlab.com/modustudio/spectrum-admin.git
```
혹은
```
git remote add upstream git@gitlab.com:modustudio/spectrum-admin.git
```
*upstream* 추가 후 *remote* 확인
```
git remote
```
결과
```
origin
upstream
```

### `push` 전 *upstream* 최신화 확인
```
git fetch upstream
```

## `gulp` 셋팅
```
npm i -D gulp gulp-plumber@1.2.0 gulp-connect@5.0.0 gulp-ejs-locals@0.0.1 gulp-sass@3.1.0 gulp-csscomb@3.0.8 gulp-autoprefixer@4.1.0 gulp-jshint@2.1.0 jshint-stylish@2.2.1 gulp-watch@5.0.0 gulp-clean@0.4.0 gulp-prettify@0.5.0
npm i -D gulp-imagemin@4.1.0
npm i -D imagemin-pngquant@5.0.1
```
`gulp -v` 으로 *CLI*, *Local* 동일 버젼 확인  

## 폴더 구조
    public/
      src/
        css/
        ejs/
        images/
        fonts/
        js/
        libs/
        scss/
        index.html(index.ejs)
    .csscomb.json
    .gitignore
    gulpfile.js
    package-lock.json
    package.json
    README.md  